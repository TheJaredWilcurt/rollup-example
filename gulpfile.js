var gulp = require('gulp');
var rollup = require('rollup-stream');
var resolve = require('rollup-plugin-node-resolve');
var common = require('rollup-plugin-commonjs');
var source = require('vinyl-source-stream');

gulp.task('rollup', function () {
    return rollup({
            entry: './_scripts/main.js',
            plugins: [ resolve(), common() ],
            format: 'iife'
        })
        .pipe(source('all.js'))
        .pipe(gulp.dest('./_scripts/'));
});

gulp.task('default', ['rollup']);
